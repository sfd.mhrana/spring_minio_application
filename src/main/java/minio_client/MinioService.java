package minio_client;

import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.Bucket;
import org.apache.commons.compress.utils.FileNameUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import utils.NumberUtilsFunctions;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class MinioService {
    MinioClient minioClient = MinioClient.builder().endpoint("http://127.0.0.1:9000").credentials("CujOYjQoVHAlozDya4v7", "oNoQ3LSUSQTBSlXPXEGY6CCeg0BeftXP02eZ3a06").build();

    protected ResponseEntity<String> uploadObject(UploadObjectModel objectModel) {
        try {
            if (this.createBucket(objectModel.bucketName)) {
                InputStream inputStream = objectModel.file.getInputStream();
                String fileName = getNewFileName(objectModel.file);
                var obj = minioClient.putObject(PutObjectArgs.builder().bucket(objectModel.bucketName).object(fileName).stream(inputStream, inputStream.available(), -1).build());
                return ResponseEntity.ok(obj.object());
            }
            return ResponseEntity.ok("Wrong");

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseEntity<InputStreamResource> getFile(GetObjectModel objectModel) {
        try {
            InputStream obj = minioClient.getObject(GetObjectArgs.builder().bucket(objectModel.bucketName).object(objectModel.objectName).build());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(minioClient.statObject(StatObjectArgs.builder().bucket(objectModel.bucketName).object(objectModel.objectName).build()).size());
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            headers.setContentDispositionFormData("attachment", objectModel.objectName);
            return new ResponseEntity<InputStreamResource>(new InputStreamResource(obj), headers, HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected boolean createBucket(String bucketName) throws InvalidResponseException {
        try {
            if (!isExistBucket(bucketName)) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            }
            return true;
        } catch (ErrorResponseException | ServerException | IOException | InvalidKeyException | XmlParserException |
                 InternalException | NoSuchAlgorithmException | InsufficientDataException e) {
            throw new RuntimeException(e);
        }
    }

    private List<Bucket> bucketList() throws InvalidResponseException {
        try {
            return minioClient.listBuckets();
        } catch (ErrorResponseException | ServerException | IOException | InvalidKeyException | XmlParserException |
                 InternalException | NoSuchAlgorithmException | InsufficientDataException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isExistBucket(String bucketName) throws InvalidResponseException {
        try {
            return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        } catch (ErrorResponseException | ServerException | IOException | InvalidKeyException | XmlParserException |
                 InternalException | NoSuchAlgorithmException | InsufficientDataException e) {
            throw new RuntimeException(e);
        }
    }

    private String getNewFileName(MultipartFile file) {
        return NumberUtilsFunctions.randomNumber() + "_" + FileNameUtils.getBaseName(file.getOriginalFilename()) + '.' + FileNameUtils.getExtension(file.getOriginalFilename());
    }


}
