package minio_client;

import org.springframework.web.multipart.MultipartFile;

public class UploadObjectModel {
    public MultipartFile file;
    public String bucketName="file";

    public UploadObjectModel(MultipartFile file,String bucketName){
        this.file=file;
        this.bucketName=bucketName;
    }

}
