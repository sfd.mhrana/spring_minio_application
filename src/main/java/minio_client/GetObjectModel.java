package minio_client;

public class GetObjectModel {

    public String bucketName = "file";
    public String objectName = "file";

    public GetObjectModel(String objectName, String bucketName) {
        this.objectName = objectName;
        this.bucketName = bucketName;
    }
}
