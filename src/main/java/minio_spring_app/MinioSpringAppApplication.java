package minio_spring_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinioSpringAppApplication {
    public static void main(String[] args) {
        SpringApplication.run(MinioSpringAppApplication.class, args);
    }

}
