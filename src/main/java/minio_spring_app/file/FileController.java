package minio_spring_app.file;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

@RestController
@RequestMapping("v1/file")
public class FileController {
    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @PostMapping(name = "insert", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> fileUpload(@RequestPart("file") MultipartFile file) {
        return fileService.addFile(file);
    }

    @GetMapping()
    public ResponseEntity<InputStreamResource> getFileObject(String fileUrl) {
        return fileService.getFileObject(fileUrl);
    }

}
