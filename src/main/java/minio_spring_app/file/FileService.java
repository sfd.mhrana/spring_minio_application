package minio_spring_app.file;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import minio_client.*;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class FileService extends MinioService {
    public ResponseEntity<String> addFile(MultipartFile file) {
        try {
            UploadObjectModel m = new UploadObjectModel(file, "file");
            return uploadObject(m);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ResponseEntity<InputStreamResource> getFileObject(String fileUrl) {
        try {
            GetObjectModel m = new GetObjectModel(fileUrl, "file");
            return getFile(m);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
